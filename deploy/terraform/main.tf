provider "google" {
  project = "aylien-terraform"
  region  = "us-central1"
  zone    = "us-central1-c"
}

resource "google_container_cluster" "primary" {
  name               = "aylien"
  initial_node_count = 2

  master_auth {
    password  = ""
    username = ""
    client_certificate_config {
      issue_client_certificate = true
    }
  }

  node_config {
    disk_size_gb = "10"
    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }

  autoscaling {
    min_node_count = 2
    max_node_count = 5
  }

  management {
    auto_repair  = true
    auto_upgrade = true
  }
}
