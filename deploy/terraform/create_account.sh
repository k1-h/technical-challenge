#!/bin/bash

export BILLING_ACCOUNT=__GCP_BILLING_ACCOUNT__
export ADMIN=aylien-terraform
export CREDS=~/.config/gcloud/${USER}-terraform.json

# Create service account and get the keys!
gcloud projects create ${ADMIN} --set-as-default
gcloud beta billing projects link ${ADMIN} --billing-account ${BILLING_ACCOUNT}
gcloud iam service-accounts create terraform --display-name "Terraform service account"
gcloud iam service-accounts keys create ${CREDS} --iam-account terraform@${ADMIN}.iam.gserviceaccount.com

# Enable API gateways required for GKE
gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable cloudbilling.googleapis.com
gcloud services enable iam.googleapis.com
gcloud services enable compute.googleapis.com
gcloud services enable container.googleapis.com

# Add neccessary roles GKE cluster to service account
gcloud projects add-iam-policy-binding ${ADMIN} --member serviceAccount:terraform@${ADMIN}.iam.gserviceaccount.com --role roles/container.admin
gcloud projects add-iam-policy-binding ${ADMIN} --member serviceAccount:terraform@${ADMIN}.iam.gserviceaccount.com --role roles/compute.admin
gcloud projects add-iam-policy-binding ${ADMIN} --member serviceAccount:terraform@${ADMIN}.iam.gserviceaccount.com --role roles/logging.logWriter
gcloud projects add-iam-policy-binding ${ADMIN} --member serviceAccount:terraform@${ADMIN}.iam.gserviceaccount.com --role roles/monitoring.metricWriter
gcloud projects add-iam-policy-binding ${ADMIN} --member serviceAccount:terraform@${ADMIN}.iam.gserviceaccount.com --role roles/iam.serviceAccountUser
