
.PHONY: clean build test push run shell
.DEFAULT_GOAL := test

REPOSITORY=""
CONTAINERNAME="aylien/techtest-py"
VERSION=latest

clean:
	for i in $(shell docker images | grep $(CONTAINERNAME) | awk '{print $$3}' | sort -u); do docker rmi -f $$i; done; \
	find . \( -name "*.pyc" -o -name "__pycache__" \) -delete

build:
	docker build -t $(CONTAINERNAME):$(VERSION) .

test: build
	-docker container rm -f techtest-py
	docker run --name techtest-py --rm -d $(CONTAINERNAME):$(VERSION)
	docker exec -it techtest-py python solver/test.py
	docker container rm -f techtest-py

push: build
	$(MAKE) test
	git status
	git add .
	git commit -m "$m"
	git push origin master

run: build
	-docker container rm -f techtest-py
	docker run -p 8080:8080 -p 8081:8081 --name techtest-py --rm $(CONTAINERNAME):$(VERSION)

shell: build
	docker run --rm -it $(CONTAINERNAME):$(VERSION) sh
