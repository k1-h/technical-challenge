## Prepare
This guide assumes that you have installed `docker`, `gcloud`, `terraform`, `kubectl`
and `helm` binaries on your machine.

## GCP
The shell script `deploy/terraform/create_account.sh` will create a project on GCP
and an account with privileges to setup a Kubernetes cluster. You should have configured
`gcloud` to have access to your GCP account and changed `__GCP_BILLING_ACCOUNT__` in `create_account.sh`
to your billing account accordingly. After that you can run this command:

```bash
cd deploy/terraform
bash create_account.sh
```

## Kubernetes cluster
The script in `deploy/terraform/main.tf` will deploy a Kubernetes cluster on GCP with
2 nodes and 10 GB disk space on each and cluster will scale up to 5 nodes.
Run this command to create a Kubernetes cluster and get credentials to connect to it.

```bash
export GOOGLE_CLOUD_KEYFILE_JSON=~/.config/gcloud/${USER}-terraform.json
terraform init
terraform apply
gcloud container clusters get-credentials aylien --zone us-central1-c
```

## Deploy
Now you can actually install the project on Kubernetes. There is a Helm chart for
this project that helps to you install it easily, but you should have install Helm
on you Kubernetes cluster as well. Following commands will install Helm and the project
with default image.

```bash
kubectl apply -f tiller-rbac.yaml
helm init --service-account tiller
cd ..
helm install techtest-py
```

Now the application is installed and you can check its status with
```bash
kubectl get pods
```

In order to test the application you can run these commands

```bash
export EXTERNAL_IP=$(kubectl get svc aylien-techtest-py -o jsonpath="{.status.loadBalancer.ingress[*].ip}")
curl -g "$EXTERNAL_IP:8080/v1/?input={%22colors%22:1,%22customers%22:2,%22demands%22:[[1,1,1],[1,1,0]]}"
curl -g "$EXTERNAL_IP:8080/v1/?input={%22colors%22:5,%22customers%22:3,%22demands%22:[[1,1,1],[2,1,0,2,0],[1,5,0]]}"
```

## Monitoring
Following command will install a simple Prometheus server with Alertmanager in GKE,
After installation Prometheus will scrape `requests_total` metrics from application pods
and it's possible to monitor this metric and create graphs from this metric.

```bash
helm install -f prometheus.yaml stable/prometheus
```

**Note:** At the moment this monitoring setup is pretty basic, in future it might expanded in
following areas:
  [] Enable persistent volume to scraped data will survive a pod restart, alternatively add a remote storage like InfluxDB as remote read/write storage to save the data for indefinite amount of time
  [] Create more replicas for Alertmanager and Prometheus to enable HA
  [] Setup alert notification in Alertmanager and create alerting rules for metrics
  [] Install Grafana and create some graphs for monitored metrics and include pods resource usage metrics (CPU, RAM, etc) in dashboard as well
  [] Add authentication to monitoring services and scrape port to avoid unauthorized access
